<?php

namespace App\Http\Controllers;

use App\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $barangs = Barang::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Barang',
            'data'    => $barangs  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
       $barangs = Barang::find($id);

        if($barangs){
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Post',
                'data'    => $barangs 
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Post ' . $id .' tidak ditemukan',
        ], 404);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'komentar' => 'required',
            'kategori_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $barangs = Barang::create([
            'nama'     => $request->nama,
            'deskripsi'   => $request->deskripsi,
            'harga'   => $request->harga,
            'komentar'   => $request->komentar,
            'kategori_id'   => $request->kategori_id,
        ]);

        //success save to database
        if($barangs) {

            return response()->json([
                'success' => true,
                'message' => 'Barang Created',
                'data'    => $barangs  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Barang Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Barang $barang)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'komentar' => 'required',
            'kategori_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $barangs = Barang::find($barang->id);

        if($barangs) {

            //update post
            $barangs->update([
                'nama'     => $request->nama,
                'deskripsi'   => $request->deskripsi,
                'harga'   => $request->harga,
                'komentar'   => $request->komentar,
                'kategori_id'   => $request->kategori_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'barang Updated',
                'data'    => $barangs  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Barang Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $barangs = Barang::findOrfail($id);

        if($barangs) {

            //delete post
            $barangs->delete();

            return response()->json([
                'success' => true,
                'message' => 'Barang Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Barang Not Found',
        ], 404);
    }
}
